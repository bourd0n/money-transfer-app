package com.bourd0n.revolut;

import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/")
public class MoneyTransferApp extends ResourceConfig {

    public MoneyTransferApp() {
        packages(getClass().getPackage().toString());
        register(new MoneyTransferAppBinder());
    }


}
