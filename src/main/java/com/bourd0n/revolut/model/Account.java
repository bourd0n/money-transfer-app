package com.bourd0n.revolut.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;

@Entity
@Table(name = "accounts")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private BigInteger id;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "currency_code")
    private String currencyCode;
    @Version
    @Column(name = "version")
    @JsonIgnore
    private int version;


    public Account() {
    }

    public Account(String currencyCode) {
        this.currencyCode = currencyCode;
        this.amount = BigDecimal.ZERO;
    }

    public Account(String currencyCode, BigDecimal amount) {
        this.currencyCode = currencyCode;
        this.amount = amount;
    }

    public BigInteger getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @JsonIgnore
    public CurrencyUnit getCurrency() {
        return Monetary.getCurrency(currencyCode);
    }

    @JsonIgnore
    public MonetaryAmount getMoney() {
        return Money.of(amount, currencyCode);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", amount=" + amount +
                ", currencyCode='" + currencyCode + '\'' +
                ", version=" + version +
                '}';
    }
}
