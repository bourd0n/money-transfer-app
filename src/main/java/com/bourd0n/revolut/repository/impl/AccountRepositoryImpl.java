package com.bourd0n.revolut.repository.impl;

import com.bourd0n.revolut.model.Account;
import com.bourd0n.revolut.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.LockModeType;
import java.math.BigInteger;

@Singleton
public class AccountRepositoryImpl implements AccountRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountRepositoryImpl.class);

    private TxUtils txUtils;

    @Inject
    public AccountRepositoryImpl(TxUtils txUtils) {
        this.txUtils = txUtils;
    }

    @Override
    public Account getById(BigInteger id) {
        return txUtils.doInTx(() -> txUtils.getEntityManager().find(Account.class, id, LockModeType.OPTIMISTIC));
    }

    @Override
    public Account save(Account account) {
        return txUtils.doInTx(() -> txUtils.getEntityManager().merge(account));
    }

}
