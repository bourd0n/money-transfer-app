package com.bourd0n.revolut.repository.impl;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.*;
import java.util.concurrent.Callable;

@Singleton
public class TxUtils {

    private EntityManagerFactory entityManagerFactory;

    private ThreadLocal<EntityManager> entityManagerHolder = ThreadLocal.withInitial(() ->
            entityManagerFactory.createEntityManager());

    @Inject
    public TxUtils(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public EntityManager getEntityManager() {
        return entityManagerHolder.get();
    }

    public <T> T doInTx(Callable<T> action) throws PersistenceException {
        EntityManager entityManager = entityManagerHolder.get();
        EntityTransaction tx = entityManager.getTransaction();
        boolean needCommit = false;
        if (!tx.isActive()) {
            needCommit = true;
            tx.begin();
        }
        T result;
        try {
            result = action.call();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } catch (Throwable e) {
            tx.rollback();
            entityManagerHolder.remove();
            throw new PersistenceException(e);
        }
        if (needCommit) {
            try {
                tx.commit();
                entityManagerHolder.remove();
            } catch (RollbackException e) {
                if (e.getCause() instanceof OptimisticLockException) {
                    throw (OptimisticLockException) e.getCause();
                } else {
                    throw e;
                }
            }
        }
        return result;
    }
}
