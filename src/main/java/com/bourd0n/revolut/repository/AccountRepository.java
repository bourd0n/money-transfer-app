package com.bourd0n.revolut.repository;

import com.bourd0n.revolut.model.Account;

import java.math.BigInteger;

public interface AccountRepository {

    Account getById(BigInteger id);

    Account save(Account account);
}
