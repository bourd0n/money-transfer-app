package com.bourd0n.revolut;

import com.bourd0n.revolut.repository.AccountRepository;
import com.bourd0n.revolut.repository.impl.AccountRepositoryImpl;
import com.bourd0n.revolut.repository.impl.TxUtils;
import com.bourd0n.revolut.service.AccountService;
import com.bourd0n.revolut.service.impl.AccountServiceImpl;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MoneyTransferAppBinder extends AbstractBinder {

    @Override
    protected void configure() {
        bind(TxUtils.class).to(TxUtils.class);
        bind(AccountRepositoryImpl.class).to(AccountRepository.class);
        bind(AccountServiceImpl.class).to(AccountService.class);
        bind(Persistence.createEntityManagerFactory("accountsUnit"))
                .to(EntityManagerFactory.class);
    }
}
