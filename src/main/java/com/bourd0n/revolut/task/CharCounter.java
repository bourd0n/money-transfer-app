package com.bourd0n.revolut.task;

import edu.umd.cs.findbugs.graph.SearchTree;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public class CharCounter {

    public static Map<Character, Long> countChars(String word) {
        if (word == null) {
            throw new IllegalArgumentException("Input word should be not null");
        }
        Map<Character, Long> charsCount = new HashMap<>();
        for (int i = 0; i < word.length(); i++) {
            Character c = word.charAt(i);
            charsCount.compute(c, (character, curCount) -> curCount == null ? 1 : curCount + 1);
        }
        return charsCount;
    }

    public static Map<Character, Long> countChars(Stream<Character> characterStream) {
        if (characterStream == null) {
            throw new IllegalArgumentException("Input stream should be not null");
        }
//        Map<Character, Long> charsCount = new HashMap<>();
//        characterStream.forEach(c -> charsCount.compute(c, (character, curCount) -> curCount == null ? 1 : curCount + 1));
        Map<Character, Long> result = characterStream.collect(Collectors.toMap(Function.identity(),
                c -> 1L, (v1, v2) -> v1 + v2));
        return result;
    }
}
