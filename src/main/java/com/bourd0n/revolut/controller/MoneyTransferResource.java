package com.bourd0n.revolut.controller;

import com.bourd0n.revolut.model.Account;

import javax.annotation.Nullable;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.math.BigInteger;

public interface MoneyTransferResource {

    /**
     * Return current account state
     *
     * @param accountId account id
     * @return Return current account state
     */
    @GET
    @Path("/{accountId}")
    @Produces(MediaType.APPLICATION_JSON)
    Account getAccount(@PathParam("accountId") BigInteger accountId);

    /**
     * Create account
     *
     * @param account account with initial amount and amount currency
     * @return created account
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Account createAccount(Account account);

    /**
     * Add money to account
     *
     * @param accountId account id to add money to
     * @param amount    amount of money to add
     * @param currency  currency of amount. Could be empty - then current account currency will be used
     * @return updated account
     */
    @POST
    @Path("/{accountId}/money/add")
    @Produces(MediaType.APPLICATION_JSON)
    Account addMoney(@PathParam("accountId") BigInteger accountId,
                     @QueryParam("amount") BigDecimal amount,
                     @QueryParam("currency") @Nullable String currency);

    /**
     * Take money from account
     *
     * @param accountId account id to take money from
     * @param amount    amount of money to take
     * @param currency  currency of amount. Could be empty - then current account currency will be used
     * @return updated account
     */
    @POST
    @Path("/{accountId}/money/take")
    @Produces(MediaType.APPLICATION_JSON)
    Account takeMoney(@PathParam("accountId") BigInteger accountId,
                      @QueryParam("amount") BigDecimal amount,
                      @QueryParam("currency") @Nullable String currency);

    /**
     * Transfer money between accounts with different currencies.
     * If 'currency' param has value of 'fromAccount'-account currency,
     * when 'amount' value will be get from 'fromAccount'-account and transferred to 'toAccount'-account with current exchange rate,
     * i.e. 'fromAccount' will transfer 'amount' money in his currency
     * <p>
     * If 'currency' param has value of 'toAccount'-account currency,
     * when 'amount' value multiplied by current exchange rate in 'fromAccount'-currency will be get 'fromAccount'-account and transferred to 'toAccount',
     * i.e. 'toAccount' will receive 'amount' money in his currency
     * <p>
     * If 'currency' param is empty - assume transfer between accounts with same currency, otherwise fail.
     *
     * @param accountFromId id of account to transfer money from
     * @param accountToId   id of account to transfer money to
     * @param amount        amount of money to transfer
     * @param currency      currency of amount. Could be currency of 'accountFrom'-account
     *                      or currency of 'accountTo'-account, or empty
     * @return new state of account from which money was transferred
     */
    @POST
    @Path("/{fromAccountId}/money/transfer")
    @Produces(MediaType.APPLICATION_JSON)
    Account transferMoney(@PathParam("fromAccountId") BigInteger accountFromId,
                          @QueryParam("toAccountId") BigInteger accountToId,
                          @QueryParam("amount") BigDecimal amount,
                          @QueryParam("currency") @Nullable String currency);
}
