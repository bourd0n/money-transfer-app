package com.bourd0n.revolut.controller.impl;

import com.bourd0n.revolut.controller.MoneyTransferResource;
import com.bourd0n.revolut.model.Account;
import com.bourd0n.revolut.service.AccountService;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.internal.guava.Preconditions;
import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.persistence.EntityNotFoundException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.math.BigInteger;

@Path("/api/account")
public class MoneyTransferResourceImpl implements MoneyTransferResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(MoneyTransferResourceImpl.class);

    private AccountService accountService;

    @Inject
    public MoneyTransferResourceImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * {@inheritDoc}
     */
    @GET
    @Path("/{accountId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Account getAccount(@PathParam("accountId") BigInteger accountId) {
        LOGGER.debug("Get Account by Id {}", accountId);
        Account account = accountService.getById(accountId);
        if (account == null) {
            throw new EntityNotFoundException("Account " + accountId + " not found");
        }
        LOGGER.debug("Get Account by id {} : Result {}", accountId, account);
        return account;
    }

    /**
     * {@inheritDoc}
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Account createAccount(Account account) {
        LOGGER.debug("Request to save account {}", account);
        Preconditions.checkArgument(account.getId() == null, "Account for creation should be without id");
        return accountService.save(account);
    }

    /**
     * {@inheritDoc}
     */
    @POST
    @Path("/{accountId}/money/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Account addMoney(@PathParam("accountId") BigInteger accountId,
                            @QueryParam("amount") BigDecimal amount,
                            @QueryParam("currency") @Nullable String currency) {
        LOGGER.debug("Request to add {} {} money to account {}", amount, currency, accountId);
        Preconditions.checkArgument(amount.compareTo(BigDecimal.ZERO) > 0, "Money amount should be > 0");
        Account account = accountService.getById(accountId);
        if (account == null) {
            throw new EntityNotFoundException("Account " + accountId + " not found");
        }
        String currencyCode = StringUtils.isEmpty(currency) ?
                account.getCurrencyCode() : currency;
        CurrencyUnit currencyUnit = Monetary.getCurrency(currencyCode);
        MonetaryAmount monetaryAmount = Money.of(amount, currencyUnit);
        return accountService.addMoney(accountId, monetaryAmount);
    }

    /**
     * {@inheritDoc}
     */
    @POST
    @Path("/{accountId}/money/take")
    @Produces(MediaType.APPLICATION_JSON)
    public Account takeMoney(@PathParam("accountId") BigInteger accountId,
                             @QueryParam("amount") BigDecimal amount,
                             @QueryParam("currency") @Nullable String currency) {
        LOGGER.debug("Request to take {} {} money from account {}", amount, currency, accountId);
        Preconditions.checkArgument(amount.compareTo(BigDecimal.ZERO) > 0, "Money amount should be > 0");
        Account account = accountService.getById(accountId);
        if (account == null) {
            throw new EntityNotFoundException("Account " + accountId + " not found");
        }
        String currencyCode = StringUtils.isEmpty(currency) ?
                account.getCurrencyCode() : currency;
        CurrencyUnit currencyUnit = Monetary.getCurrency(currencyCode);
        MonetaryAmount monetaryAmount = Money.of(amount, currencyUnit);
        return accountService.takeMoney(accountId, monetaryAmount);
    }

    /**
     * {@inheritDoc}
     */
    @POST
    @Path("/{fromAccountId}/money/transfer")
    @Produces(MediaType.APPLICATION_JSON)
    public Account transferMoney(@PathParam("fromAccountId") BigInteger accountFromId,
                                 @QueryParam("toAccountId") BigInteger accountToId,
                                 @QueryParam("amount") BigDecimal amount,
                                 @QueryParam("currency") @Nullable String currency) {
        LOGGER.debug("Request to transfer {} {} money from account {} to account {}", amount, currency, accountFromId, accountToId);
        Preconditions.checkArgument(amount.compareTo(BigDecimal.ZERO) > 0, "Money amount should be > 0");
        Account accountFrom = accountService.getById(accountFromId);
        if (accountFrom == null) {
            throw new EntityNotFoundException("Account " + accountFromId + " not found");
        }
        Account accountTo = accountService.getById(accountToId);
        if (accountTo == null) {
            throw new EntityNotFoundException("Account " + accountToId + " not found");
        }

        CurrencyUnit fromCurrency = accountFrom.getCurrency();
        CurrencyUnit toCurrency = accountTo.getCurrency();

        if (StringUtils.isEmpty(currency)) {
            if (!fromCurrency.equals(toCurrency)) {
                throw new IllegalStateException(String.format("Accounts '%s'(%s) and '%s'(%s) has different currencies, " +
                        "so need to provide target currency for transfer", accountFromId, fromCurrency, accountToId, toCurrency));
            }
            MonetaryAmount monetaryAmount = Money.of(amount, fromCurrency);
            return accountService.transferMoneyFrom(accountFromId, accountToId, monetaryAmount);
        } else if (currency.equals(fromCurrency.getCurrencyCode())) {
            MonetaryAmount monetaryAmount = Money.of(amount, fromCurrency);
            return accountService.transferMoneyFrom(accountFromId, accountToId, monetaryAmount);
        } else if (currency.equals(toCurrency.getCurrencyCode())) {
            MonetaryAmount monetaryAmount = Money.of(amount, toCurrency);
            return accountService.transferMoneyTo(accountFromId, accountToId, monetaryAmount);
        } else {
            throw new IllegalArgumentException(String.format("Illegal currency '%s' passed. Should be empty " +
                    "or equals currency of from-account(id='%s') or currency of to-account(id='%s')", currency, accountFromId, accountToId));
        }
    }
}
