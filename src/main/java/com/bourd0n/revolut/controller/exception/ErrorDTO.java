package com.bourd0n.revolut.controller.exception;

import static com.bourd0n.revolut.controller.exception.ErrorCode.UNKNOWN_EXCEPTION;

public class ErrorDTO {
    private String code;
    private String message;

    public ErrorDTO() {
    }

    public ErrorDTO(ErrorCode code, String message) {
        this.code = code.getCode();
        this.message = message;
    }

    public ErrorDTO(ErrorCode code, Throwable cause) {
        this.code = code.getCode();
        this.message = cause.getMessage();
    }


    public ErrorDTO(Throwable cause) {
        this(UNKNOWN_EXCEPTION, cause.getMessage());
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
