package com.bourd0n.revolut.controller.exception;

/**
 * Error codes
 */
public enum ErrorCode {
    UNKNOWN_EXCEPTION("0000"),
    ENTITY_NOT_FOUND_EXCEPTION("0001"),
    CONFLICT_UPDATE_EXCEPTION("0002"),
    PERSISTENCE_EXCEPTION("0003"),
    BAD_REQUEST_EXCEPTION("0004");

    String code;

    ErrorCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
