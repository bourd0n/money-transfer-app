package com.bourd0n.revolut.controller.exception.mapper;

import com.bourd0n.revolut.controller.exception.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static com.bourd0n.revolut.controller.exception.ErrorCode.ENTITY_NOT_FOUND_EXCEPTION;

@Provider
public class EntityNotFoundExceptionMapper implements ExceptionMapper<EntityNotFoundException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntityNotFoundExceptionMapper.class);

    public Response toResponse(EntityNotFoundException exception) {
        LOGGER.error("[" + ENTITY_NOT_FOUND_EXCEPTION + "] error occurred", exception);
        return Response.status(Response.Status.NOT_FOUND).
                entity(new ErrorDTO(ENTITY_NOT_FOUND_EXCEPTION, exception))
                .build();
    }
}