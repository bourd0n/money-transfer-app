package com.bourd0n.revolut.controller.exception.mapper;

import com.bourd0n.revolut.controller.exception.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.OptimisticLockException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static com.bourd0n.revolut.controller.exception.ErrorCode.CONFLICT_UPDATE_EXCEPTION;

@Provider
public class OptimisticLockExceptionMapper implements ExceptionMapper<OptimisticLockException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OptimisticLockExceptionMapper.class);

    @Override
    public Response toResponse(OptimisticLockException exception) {
        LOGGER.error("[" + CONFLICT_UPDATE_EXCEPTION + "] exception occurred", exception);
        return Response.status(Response.Status.CONFLICT)
                .entity(new ErrorDTO(CONFLICT_UPDATE_EXCEPTION, "Conflict during updating because of subsequent update. Please retry"))
                .build();
    }
}
