package com.bourd0n.revolut.controller.exception.mapper;

import com.bourd0n.revolut.controller.exception.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static com.bourd0n.revolut.controller.exception.ErrorCode.BAD_REQUEST_EXCEPTION;

@Provider
public class IllegalStateExceptionMapper implements ExceptionMapper<IllegalStateException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IllegalStateExceptionMapper.class);

    @Override
    public Response toResponse(IllegalStateException exception) {
        LOGGER.error("[" + BAD_REQUEST_EXCEPTION + "] error occurred", exception);
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorDTO(BAD_REQUEST_EXCEPTION, exception))
                .build();
    }
}
