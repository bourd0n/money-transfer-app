package com.bourd0n.revolut.controller.exception.mapper;

import com.bourd0n.revolut.controller.exception.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static com.bourd0n.revolut.controller.exception.ErrorCode.PERSISTENCE_EXCEPTION;

@Provider
public class PersistenceExceptionMapper implements ExceptionMapper<PersistenceException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceExceptionMapper.class);

    @Override
    public Response toResponse(PersistenceException exception) {
        LOGGER.error("[" + PERSISTENCE_EXCEPTION + "] exception occurred", exception);
        return Response.serverError()
                .entity(new ErrorDTO(PERSISTENCE_EXCEPTION,"Some error during saving. Please retry"))
                .build();
    }
}
