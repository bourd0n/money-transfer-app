package com.bourd0n.revolut.controller.exception.mapper;

import com.bourd0n.revolut.controller.exception.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static com.bourd0n.revolut.controller.exception.ErrorCode.BAD_REQUEST_EXCEPTION;

@Provider
public class IllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IllegalArgumentExceptionMapper.class);

    @Override
    public Response toResponse(IllegalArgumentException exception) {
        LOGGER.error("[" + BAD_REQUEST_EXCEPTION + "] error occurred", exception);
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorDTO(BAD_REQUEST_EXCEPTION, exception))
                .build();
    }
}
