package com.bourd0n.revolut.controller.exception.mapper;

import com.bourd0n.revolut.controller.exception.ErrorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static com.bourd0n.revolut.controller.exception.ErrorCode.UNKNOWN_EXCEPTION;


/**
 * Generic exception mapper
 */
@Provider
public class ThrowableExceptionMapper implements ExceptionMapper<Throwable> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThrowableExceptionMapper.class);

    @Override
    public Response toResponse(Throwable exception) {
        LOGGER.error("[" + UNKNOWN_EXCEPTION + "] exception occurred", exception);
        return Response.serverError()
                .entity(new ErrorDTO(exception))
                .build();
    }
}
