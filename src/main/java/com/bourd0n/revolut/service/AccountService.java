package com.bourd0n.revolut.service;

import com.bourd0n.revolut.model.Account;

import javax.money.MonetaryAmount;
import java.math.BigInteger;

public interface AccountService {

    /**
     * Get account by specified id
     *
     * @param id account id
     * @return account state
     */
    Account getById(BigInteger id);

    /**
     * Save account
     *
     * @param account account state to save
     * @return saved account
     */
    Account save(Account account);

    /**
     * Transfer 'monetaryAmount' money from 'accountFrom' to 'accountTo' with currency conversion
     *
     * @param accountFrom    account id to take money from
     * @param accountTo      account id to transfer money to
     * @param monetaryAmount amount of transfer
     * @return updated 'accountFrom' state
     */
    Account transferMoneyFrom(BigInteger accountFrom, BigInteger accountTo, MonetaryAmount monetaryAmount);

    /**
     * Transfer 'monetaryAmount' to 'accountTo' from 'accountFrom' with currency conversion
     *
     * @param accountFrom    account id to take money from
     * @param accountTo      account id to transfer money to
     * @param monetaryAmount amount of transfer
     * @return updated 'accountFrom' state
     */
    Account transferMoneyTo(BigInteger accountFrom, BigInteger accountTo, MonetaryAmount monetaryAmount);

    /**
     * Add money to account
     *
     * @param accountTo      account id to transfer money to
     * @param monetaryAmount amount to add
     * @return updated 'accountFrom' state
     */
    Account addMoney(BigInteger accountTo, MonetaryAmount monetaryAmount);

    /**
     * Take money from account
     *
     * @param accountFrom    account id to take money from
     * @param monetaryAmount amount to take
     * @return updated 'accountFrom' state
     */
    Account takeMoney(BigInteger accountFrom, MonetaryAmount monetaryAmount);
}
