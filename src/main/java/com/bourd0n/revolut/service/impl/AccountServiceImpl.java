package com.bourd0n.revolut.service.impl;

import com.bourd0n.revolut.model.Account;
import com.bourd0n.revolut.repository.AccountRepository;
import com.bourd0n.revolut.repository.impl.TxUtils;
import com.bourd0n.revolut.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;
import java.math.BigDecimal;
import java.math.BigInteger;

@Singleton
public class AccountServiceImpl implements AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

    private AccountRepository accountRepository;
    private TxUtils txUtils;

    @Inject
    public AccountServiceImpl(AccountRepository accountRepository, TxUtils txUtils) {
        this.accountRepository = accountRepository;
        this.txUtils = txUtils;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account getById(BigInteger id) {
        return accountRepository.getById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account save(Account account) {
        return accountRepository.save(account);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account transferMoneyFrom(BigInteger accountFromId, BigInteger accountToId, MonetaryAmount monetaryAmount) {
        return txUtils.doInTx(() -> {
            Account accountTo = accountRepository.getById(accountToId);
            Account accountFrom = accountRepository.getById(accountFromId);
            MonetaryAmount moneyFrom = accountFrom.getMoney();
            MonetaryAmount moneyTo = accountTo.getMoney();
            MonetaryAmount monetaryAmountToGet = monetaryAmount;
            MonetaryAmount monetaryAmountToAdd = monetaryAmount;
            if (!accountTo.getCurrency().equals(monetaryAmount.getCurrency())) {
                CurrencyConversion conversion = MonetaryConversions.getConversion(accountTo.getCurrency());
                monetaryAmountToAdd = monetaryAmount.with(conversion);
                LOGGER.info("Get {} from account {} and move to account {} with conversion to {}. Final amount to add: {}",
                        monetaryAmount, accountFrom.getId(), accountTo.getId(), accountTo.getCurrency(), monetaryAmountToAdd);
            }
            MonetaryAmount resultAmountForFromAccount = moneyFrom.subtract(monetaryAmountToGet);
            if (resultAmountForFromAccount.isNegative()) {
                throw new IllegalStateException("Account " + accountFromId + " can transfer '" + monetaryAmount + "' as it don't have such amount");
            }
            MonetaryAmount resultAmountForToAccount = moneyTo.add(monetaryAmountToAdd);
            accountTo.setAmount(resultAmountForToAccount.getNumber().numberValue(BigDecimal.class));
            accountFrom.setAmount(resultAmountForFromAccount.getNumber().numberValue(BigDecimal.class));
            Account modifiedAccountFrom = accountRepository.save(accountFrom);
            Account modifiedAccountTo = accountRepository.save(accountTo);
            return modifiedAccountFrom;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account transferMoneyTo(BigInteger accountFromId, BigInteger accountToId, MonetaryAmount monetaryAmount) {
        return txUtils.doInTx(() -> {
            Account accountTo = accountRepository.getById(accountToId);
            Account accountFrom = accountRepository.getById(accountFromId);
            MonetaryAmount moneyFrom = accountFrom.getMoney();
            MonetaryAmount moneyTo = accountTo.getMoney();
            MonetaryAmount monetaryAmountToGet = monetaryAmount;
            MonetaryAmount monetaryAmountToAdd = monetaryAmount;
            if (!accountFrom.getCurrency().equals(monetaryAmount.getCurrency())) {
                CurrencyConversion conversion = MonetaryConversions.getConversion(accountFrom.getCurrency());
                monetaryAmountToGet = monetaryAmount.with(conversion);
                LOGGER.info("Get {} from account {} with conversion to {} and move to account {}. Final amount to get: {}",
                        monetaryAmount, accountFrom.getId(), accountTo.getId(), accountTo.getCurrency(), monetaryAmountToGet);
            }
            MonetaryAmount resultAmountForFromAccount = moneyFrom.subtract(monetaryAmountToGet);
            if (resultAmountForFromAccount.isNegative()) {
                throw new IllegalStateException("Account " + accountFromId + " can transfer '" + monetaryAmount + "' as it don't have such amount");
            }
            MonetaryAmount resultAmountForToAccount = moneyTo.add(monetaryAmountToAdd);
            accountTo.setAmount(resultAmountForToAccount.getNumber().numberValue(BigDecimal.class));
            accountFrom.setAmount(resultAmountForFromAccount.getNumber().numberValue(BigDecimal.class));
            Account modifiedAccountFrom = accountRepository.save(accountFrom);
            Account modifiedAccountTo = accountRepository.save(accountTo);
            return modifiedAccountFrom;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account addMoney(BigInteger accountToId, MonetaryAmount monetaryAmount) {
        return txUtils.doInTx(() -> {
            Account accountTo = accountRepository.getById(accountToId);
            MonetaryAmount money = accountTo.getMoney();
            MonetaryAmount monetaryAmountToAdd = monetaryAmount;
            if (!accountTo.getCurrency().equals(monetaryAmount.getCurrency())) {
                CurrencyConversion conversion = MonetaryConversions.getConversion(accountTo.getCurrency());
                monetaryAmountToAdd = monetaryAmount.with(conversion);
                LOGGER.info("Add {} to account {} with conversion to {}. Final amount: {}",
                        monetaryAmount, accountTo.getId(), accountTo.getCurrency(), monetaryAmountToAdd);
            }
            MonetaryAmount resultAmount = money.add(monetaryAmountToAdd);
            accountTo.setAmount(resultAmount.getNumber().numberValue(BigDecimal.class));
            Account modifiedAccount = accountRepository.save(accountTo);
            return modifiedAccount;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account takeMoney(BigInteger accountFromId, MonetaryAmount monetaryAmount) {
        return txUtils.doInTx(() -> {
            Account accountFrom = accountRepository.getById(accountFromId);
            MonetaryAmount money = accountFrom.getMoney();
            MonetaryAmount monetaryAmountToGet = monetaryAmount;
            if (!accountFrom.getCurrency().equals(monetaryAmount.getCurrency())) {
                CurrencyConversion conversion = MonetaryConversions.getConversion(accountFrom.getCurrency());
                monetaryAmountToGet = monetaryAmount.with(conversion);
                LOGGER.info("Add {} to account {} with conversion to {} with rate {}. Final amount: {}",
                        monetaryAmount, accountFrom.getId(), accountFrom.getCurrency(),
                        conversion.getExchangeRate(monetaryAmount).getFactor(), monetaryAmountToGet);
            }
            MonetaryAmount resultAmount = money.subtract(monetaryAmountToGet);
            if (resultAmount.isNegative()) {
                throw new IllegalStateException("Account " + accountFromId + " can get '" + monetaryAmount + "' as it don't have such amount");
            }
            accountFrom.setAmount(resultAmount.getNumber().numberValue(BigDecimal.class));
            Account modifiedAccount = accountRepository.save(accountFrom);
            return modifiedAccount;
        });
    }
}
