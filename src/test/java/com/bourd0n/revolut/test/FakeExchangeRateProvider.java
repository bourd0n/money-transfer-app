package com.bourd0n.revolut.test;

import org.javamoney.moneta.convert.ExchangeRateBuilder;
import org.javamoney.moneta.spi.AbstractRateProvider;
import org.javamoney.moneta.spi.DefaultNumberValue;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.convert.*;
import java.util.Currency;

public class FakeExchangeRateProvider extends AbstractRateProvider {

    private static final ProviderContext CONTEXT =
            ProviderContextBuilder.of("CustomTestProvider", RateType.HISTORIC, RateType.DEFERRED)
                    .set("providerDescription", "European Central Bank (last 90 days)").set("days", 90).build();

    public FakeExchangeRateProvider() {
        super(CONTEXT);
    }

    //1 EUR = 2 USD
    //1 USD = 0.5 EUR
    @Override
    public ExchangeRate getExchangeRate(ConversionQuery conversionQuery) {
        if (conversionQuery.getBaseCurrency().equals(Monetary.getCurrency("EUR"))) {
            return new ExchangeRateBuilder("com.bourd0n.revolut.test.FakeExchangeRateProvider", RateType.ANY)
                    .setBase(Monetary.getCurrency("EUR"))
                    .setTerm(Monetary.getCurrency("USD"))
                    .setFactor(new DefaultNumberValue(2))
                    .build();
        } else {
            return new ExchangeRateBuilder("com.bourd0n.revolut.test.FakeExchangeRateProvider", RateType.ANY)
                    .setBase(Monetary.getCurrency("USD"))
                    .setTerm(Monetary.getCurrency("EUR"))
                    .setFactor(new DefaultNumberValue(0.5))
                    .build();
        }
    }
}
