package com.bourd0n.revolut.test;

import org.javamoney.moneta.convert.internal.DefaultMonetaryConversionsSingletonSpi;

import javax.money.convert.ConversionQuery;
import javax.money.convert.ExchangeRateProvider;

/**
 * For tests
 */
public class TestMonetaryConversionsSingletonSpi extends DefaultMonetaryConversionsSingletonSpi {

    @Override
    public ExchangeRateProvider getExchangeRateProvider(ConversionQuery conversionQuery) {
        return new FakeExchangeRateProvider();
    }
}
