package com.bourd0n.revolut.task;

import org.junit.Test;

import java.util.Map;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CharCounterTest {

    public void testNullWord() {

    }

    @Test
    public void testWordWithoutCharRepeats() {
        String word = "machine";
        Map<Character, Long> charCounts = CharCounter.countChars(word);
        assertNotNull(charCounts);
        assertEquals(7, charCounts.size());
        assertEquals(1L, (long) charCounts.get('m'));
        assertEquals(1L, (long) charCounts.get('a'));
        assertEquals(1L, (long) charCounts.get('c'));
        assertEquals(1L, (long) charCounts.get('h'));
        assertEquals(1L, (long) charCounts.get('i'));
        assertEquals(1L, (long) charCounts.get('n'));
        assertEquals(1L, (long) charCounts.get('e'));
    }

    @Test
    public void testWordWithCharRepeats() {
        String word = "appplee";
        Map<Character, Long> charCounts = CharCounter.countChars(word);
        assertNotNull(charCounts);
        assertEquals(4, charCounts.size());
        assertEquals(1L, (long) charCounts.get('a'));
        assertEquals(3L, (long) charCounts.get('p'));
        assertEquals(1L, (long) charCounts.get('l'));
        assertEquals(2L, (long) charCounts.get('e'));
    }

    @Test
    public void testWordWithOneChar() {
        String word = "a";
        Map<Character, Long> charCounts = CharCounter.countChars(word);
        assertNotNull(charCounts);
        assertEquals(1, charCounts.size());
        assertEquals(1L, (long) charCounts.get('a'));
    }

    /**
     * Stream tests
     */

    @Test
    public void testStreamWithoutCharRepeats() {
        Stream<Character> word = "machine".chars().mapToObj(value -> (char) value);
        Map<Character, Long> charCounts = CharCounter.countChars(word);
        assertNotNull(charCounts);
        assertEquals(7, charCounts.size());
        assertEquals(1L, (long) charCounts.get('m'));
        assertEquals(1L, (long) charCounts.get('a'));
        assertEquals(1L, (long) charCounts.get('c'));
        assertEquals(1L, (long) charCounts.get('h'));
        assertEquals(1L, (long) charCounts.get('i'));
        assertEquals(1L, (long) charCounts.get('n'));
        assertEquals(1L, (long) charCounts.get('e'));
    }

    @Test
    public void testStreamWithCharRepeats() {
        Stream<Character> word = "appplee".chars().mapToObj(value -> (char) value);
        Map<Character, Long> charCounts = CharCounter.countChars(word);
        assertNotNull(charCounts);
        assertEquals(4, charCounts.size());
        assertEquals(1L, (long) charCounts.get('a'));
        assertEquals(3L, (long) charCounts.get('p'));
        assertEquals(1L, (long) charCounts.get('l'));
        assertEquals(2L, (long) charCounts.get('e'));
    }

    @Test
    public void testStreamWithOneChar() {
        Stream<Character> word = "a".chars().mapToObj(value -> (char) value);
        Map<Character, Long> charCounts = CharCounter.countChars(word);
        assertNotNull(charCounts);
        assertEquals(1, charCounts.size());
        assertEquals(1L, (long) charCounts.get('a'));
    }

    @Test
    public void testBigStream() {
        Stream<Character> word = Stream.generate(() -> 'a').limit(800000000L);
        Map<Character, Long> charCounts = CharCounter.countChars(word);
        assertNotNull(charCounts);
        assertEquals(1, charCounts.size());
        assertEquals(800000000L, (long) charCounts.get('a'));
    }

}
