package com.bourd0n.revolut.service;

import com.bourd0n.revolut.MoneyTransferApp;
import com.bourd0n.revolut.MoneyTransferAppBinder;
import com.bourd0n.revolut.controller.exception.ErrorCode;
import com.bourd0n.revolut.controller.exception.ErrorDTO;
import com.bourd0n.revolut.model.Account;
import org.glassfish.jersey.internal.inject.InjectionManager;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spi.Container;
import org.glassfish.jersey.server.spi.ContainerLifecycleListener;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class MoneyTransferResourceTest extends JerseyTest {

    private AccountService accountService;

    @Override
    protected Application configure() {
        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.packages(MoneyTransferApp.class.getPackage().toString());
        resourceConfig.register(new MoneyTransferAppBinder());
        resourceConfig.register(new ContainerLifecycleListener() {
            public void onStartup(Container container) {
                //maybe should exists more easy way
                InjectionManager injectionManager = container.getApplicationHandler().getInjectionManager();
                accountService = injectionManager.getInstance(AccountService.class);
            }

            public void onReload(Container container) {/*...*/}

            public void onShutdown(Container container) {/*...*/}
        });
        return resourceConfig;
    }

    @Test
    public void testSaveAccount() {
        final Account accountEntity = target("/api/account/").request()
                .post(Entity.entity(new Account("USD", BigDecimal.TEN), MediaType.APPLICATION_JSON_TYPE), Account.class);
        assertThat(accountEntity).isNotNull();
        assertThat(accountEntity.getAmount()).isEqualByComparingTo(BigDecimal.TEN);
        assertThat(accountEntity.getCurrencyCode()).isEqualTo("USD");
        Account accountFromDb = accountService.getById(accountEntity.getId());
        assertThat(accountFromDb).isNotNull();
    }

    @Test
    public void testGetAccount() {
        Account account = accountService.save(new Account("USD", BigDecimal.TEN));
        final Account accountResponse = target("/api/account/" + account.getId()).request().get(Account.class);
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getAmount()).isEqualByComparingTo(BigDecimal.TEN);
        assertThat(accountResponse.getCurrencyCode()).isEqualTo("USD");
    }

    @Test
    public void testAddMoneyToAccountWithoutCurrency() {
        Account account = accountService.save(new Account("USD", BigDecimal.TEN));
        final Account accountResponse = target("/api/account/" + account.getId() + "/money/add")
                .queryParam("amount", 5)
                .request()
                .post(Entity.json(null), Account.class);
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getAmount()).isEqualByComparingTo(new BigDecimal("15"));
        assertThat(accountResponse.getCurrencyCode()).isEqualTo("USD");
    }

    @Test
    public void testAddMoneyToAccountWithCurrency() {
        Account account = accountService.save(new Account("USD", BigDecimal.TEN));
        final Account accountResponse = target("/api/account/" + account.getId() + "/money/add")
                .queryParam("amount", 5)
                .queryParam("currency", "EUR")
                .request()
                .post(Entity.json(null), Account.class);
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getAmount()).isEqualByComparingTo(new BigDecimal("20"));
        assertThat(accountResponse.getCurrencyCode()).isEqualTo("USD");
    }

    @Test
    public void testTakeMoneyFromAccountWithCurrency() {
        Account account = accountService.save(new Account("USD", BigDecimal.TEN));
        final Account accountResponse = target("/api/account/" + account.getId() + "/money/take")
                .queryParam("amount", 5)
                .queryParam("currency", "EUR")
                .request()
                .post(Entity.json(null), Account.class);
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getAmount()).isEqualByComparingTo(BigDecimal.ZERO);
        assertThat(accountResponse.getCurrencyCode()).isEqualTo("USD");
    }

    @Test
    public void testTakeMoneyFromAccountWithoutCurrency() {
        Account account = accountService.save(new Account("USD", BigDecimal.TEN));
        final Account accountResponse = target("/api/account/" + account.getId() + "/money/take")
                .queryParam("amount", 5)
                .request()
                .post(Entity.json(null), Account.class);
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getAmount()).isEqualByComparingTo(new BigDecimal("5"));
        assertThat(accountResponse.getCurrencyCode()).isEqualTo("USD");
    }

    @Test
    public void testTakeMoreMoneyThanAccountHave() {
        Account account = accountService.save(new Account("USD", BigDecimal.TEN));
        final Response accountResponse = target("/api/account/" + account.getId() + "/money/take")
                .queryParam("amount", 6)
                .queryParam("currency", "EUR")
                .request()
                .post(Entity.json(null));
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getStatusInfo().toEnum()).isEqualTo(Response.Status.BAD_REQUEST);
        assertThat(accountResponse.readEntity(ErrorDTO.class).getCode()).isEqualTo(ErrorCode.BAD_REQUEST_EXCEPTION.getCode());
    }

    @Test
    public void testTransferMoneyWithoutCurrency() {
        Account accountFrom = accountService.save(new Account("USD", BigDecimal.TEN));
        Account accountTo = accountService.save(new Account("USD", BigDecimal.TEN));
        final Account accountResponse = target("/api/account/" + accountFrom.getId() + "/money/transfer")
                .queryParam("amount", 6)
                .queryParam("toAccountId", accountTo.getId())
                .request()
                .post(Entity.json(null), Account.class);
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getAmount()).isEqualByComparingTo(new BigDecimal("4"));

        Account updatedAccountFrom = accountService.getById(accountFrom.getId());
        assertThat(updatedAccountFrom.getAmount()).isEqualByComparingTo(new BigDecimal("4"));
        Account updatedAccountTo = accountService.getById(accountTo.getId());
        assertThat(updatedAccountTo.getAmount()).isEqualByComparingTo(new BigDecimal("16"));
    }

    @Test
    public void testTransferMoneyWithoutCurrencyAndDifferentCurrencies() {
        Account accountFrom = accountService.save(new Account("USD", BigDecimal.TEN));
        Account accountTo = accountService.save(new Account("EUR", BigDecimal.TEN));
        final Response accountResponse = target("/api/account/" + accountFrom.getId() + "/money/transfer")
                .queryParam("amount", 6)
                .queryParam("toAccountId", accountTo.getId())
                .request()
                .post(Entity.json(null));
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getStatusInfo().toEnum()).isEqualTo(Response.Status.BAD_REQUEST);
        //accounts should be not updated
        Account updatedAccountFrom = accountService.getById(accountFrom.getId());
        assertThat(updatedAccountFrom.getAmount()).isEqualByComparingTo(BigDecimal.TEN);
        Account updatedAccountTo = accountService.getById(accountTo.getId());
        assertThat(updatedAccountTo.getAmount()).isEqualByComparingTo(BigDecimal.TEN);
    }

    @Test
    public void testTransferMoneyWithFromAccountCurrency() {
        Account accountFrom = accountService.save(new Account("USD", BigDecimal.TEN));
        Account accountTo = accountService.save(new Account("EUR", BigDecimal.TEN));
        final Account accountResponse = target("/api/account/" + accountFrom.getId() + "/money/transfer")
                .queryParam("amount", 4)
                .queryParam("toAccountId", accountTo.getId())
                .queryParam("currency", "USD")
                .request()
                .post(Entity.json(null), Account.class);
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getAmount()).isEqualByComparingTo(new BigDecimal("6"));

        Account updatedAccountFrom = accountService.getById(accountFrom.getId());
        assertThat(updatedAccountFrom.getAmount()).isEqualByComparingTo(new BigDecimal("6"));
        Account updatedAccountTo = accountService.getById(accountTo.getId());
        assertThat(updatedAccountTo.getAmount()).isEqualByComparingTo(new BigDecimal("12"));
    }

    @Test
    public void testTransferMoneyWithToAccountCurrency() {
        Account accountFrom = accountService.save(new Account("USD", BigDecimal.TEN));
        Account accountTo = accountService.save(new Account("EUR", BigDecimal.TEN));
        final Account accountResponse = target("/api/account/" + accountFrom.getId() + "/money/transfer")
                .queryParam("amount", 4)
                .queryParam("toAccountId", accountTo.getId())
                .queryParam("currency", "EUR")
                .request()
                .post(Entity.json(null), Account.class);
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getAmount()).isEqualByComparingTo(new BigDecimal("2"));

        Account updatedAccountFrom = accountService.getById(accountFrom.getId());
        assertThat(updatedAccountFrom.getAmount()).isEqualByComparingTo(new BigDecimal("2"));
        Account updatedAccountTo = accountService.getById(accountTo.getId());
        assertThat(updatedAccountTo.getAmount()).isEqualByComparingTo(new BigDecimal("14"));
    }

    @Test
    public void testTransferMoneyWithRandomCurrency() {
        Account accountFrom = accountService.save(new Account("USD", BigDecimal.TEN));
        Account accountTo = accountService.save(new Account("EUR", BigDecimal.TEN));
        final Response accountResponse = target("/api/account/" + accountFrom.getId() + "/money/transfer")
                .queryParam("amount", 4)
                .queryParam("toAccountId", accountTo.getId())
                .queryParam("currency", "RUB")
                .request()
                .post(Entity.json(null));
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse).isNotNull();
        assertThat(accountResponse.getStatusInfo().toEnum()).isEqualTo(Response.Status.BAD_REQUEST);
        //accounts should be not updated
        Account updatedAccountFrom = accountService.getById(accountFrom.getId());
        assertThat(updatedAccountFrom.getAmount()).isEqualByComparingTo(BigDecimal.TEN);
        Account updatedAccountTo = accountService.getById(accountTo.getId());
        assertThat(updatedAccountTo.getAmount()).isEqualByComparingTo(BigDecimal.TEN);
    }



}
