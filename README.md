# Money Transfer App

Application with REST API for money transfers between accounts with regard to different currencies.

[Task Description](Backend%20Developer%20Test%20–%20Revolut.pdf)

## How To Run

To run application perform:

`./gradlew appRun`

Application will be available at [http://localhost:8080](http://localhost:8080)

For tests performing: `./gradlew test` and see result at [report](build/reports/tests/test/index.html)

## REST API Description

See `com.bourd0n.revolut.controller.MoneyTransferResource` class with javadocs for more details

Also can import [Postman collection](money%20transfer%20app.postman_collection.json) with examples of REST requests.

### Add Account

```
POST /api/account
{
   "amount": "0",
   "currencyCode": "USD"
}
```

### Get Account

```
GET /api/account/{id}
```

### Add Money

With default currency:

```
POST /api/account/{id}/money/add?amount=10
```

With custom currency:

```
POST /api/account/{id}/money/add?amount=10&currency=EUR
```

### Take Money

With default currency:

```
POST /api/account/{id}/money/take?amount=10
```

With custom currency:

```
POST /api/account/{id}/money/take?amount=10&currency=EUR
```

### Transfer Money Between Accounts

```
POST /api/account/{fromAccountId}/money/transfer?toAccountId={toAccountId}&amount=10&currency=EUR
```


